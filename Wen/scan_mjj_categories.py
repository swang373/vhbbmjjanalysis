#!/usr/bin/env python
import itertools
import os

import numpy
import pandas
import wurlitzer

from utils import autocategorizer, makedirs
from utils.parsing import parse_mjj_category_trees


def suggest_categories(x, features, n_categories, metric='poisson', n_minbkg=100, smooth_bkg=False, outdir=None):
    if metric == 'poisson':
        scorer = autocategorizer.PoissonSignificance(0, 0, n_minbkg, False, False, smooth_bkg)
    elif metric == 'asimov':
        scorer = autocategorizer.AsimovSignificance(0, 0, n_minbkg, False, False, smooth_bkg)
    else:
        raise ValueError("Expected 'poisson' or 'asimov', found {0!s}".format(metric))

    filename = 'mjj_{0!s}_{1}_{2!s}_{3!r}'.format(n_categories, metric, n_minbkg, smooth_bkg)
    filename = filename.replace('.', 'p') + '.xml'
    if outdir is None:
        outdir = 'mjj_categories'
    makedirs(outdir)
    outfile = os.path.join(outdir, filename)

    tree = autocategorizer.Tree(x, 20)
    tree.setFeatureNames(features)
    with wurlitzer.pipes() as (stdout, stderr):
        tree.buildTree(n_categories, scorer)

    try:
        tree.saveToXML(outfile)
    except SystemError:
        if os.path.isfile(outfile):
            os.remove(outfile)


if __name__ == '__main__':

    features = autocategorizer.Features('WenMasslessDNN')
    events = autocategorizer.Events()
    autocategorizer.loadEvents(
        events=events,
        features=features,
        src='autocat_input.root',
        treename='Events',
        aliases={'bin': 'mjj_bin_index'},
    )
    print '  /// Loaded {0!s} training events'.format(events.size())

    N_CATEGORIES = range(2, 6)
    METRICS = ['asimov', 'poisson']
    N_MINBKG = [20, 30,  50, 70, 100, 120, 130, 150, 170, 200, 220, 230, 250]
    SMOOTH_BKG = [True, False]

    trial_params = itertools.product(N_CATEGORIES, METRICS, N_MINBKG, SMOOTH_BKG)

    for n_categories, metric, n_minbkg, smooth_bkg in trial_params:
        print n_categories, metric, n_minbkg, smooth_bkg
        suggest_categories(events, features, n_categories, metric, n_minbkg, smooth_bkg, outdir='WenMjjCategories')

    scan_results = parse_mjj_category_trees('WenMjjCategories', bounds=(0.0, 1.0))
    scan_results['categories'] = scan_results['categories'].astype(object)
    scan_results['metric'] = scan_results['metric'].astype(str)
    scan_results.to_html('scan_results_Wen.html')
    with pandas.HDFStore('scan_results_Wen.h5') as store:
        store['scan_results'] = scan_results

