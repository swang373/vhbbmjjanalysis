#!/usr/bin/env python
import glob
import os

import concurrent.futures
import numpy
import pandas
import root_pandas
import uproot

from utils import makedirs


def is_signal(event):
    """Label whether a sample is signal (1), background (0),
    or data (-1) for the autocategorizer.
    """
    # WminusH125_powheg = -12501
    # WplusH125_powheg = -12500
    if event.sampleIndex in {-12501, -12500}:
        return 1
    elif event.sampleIndex == 0:
        return -1
    else:
        return 0


def mjj_bin_index(event):
    """Label which mjj bin an event falls under for the autocategorizer.
    Note the rare appearance of the for-else construct in Python!
    """
    bin_edges = range(60, 165, 5)
    for i in range(len(bin_edges) - 1):
        if bin_edges[i] <= event['H_mass'] < bin_edges[i+1]:
            return i
    else:
        return -1

def preprocess_for_autocat(path, friend, outdir):
    """Preprocess a 2017 ntuple and attach the massless evaluated DNN from the friend tree."""

    SELECTION = (
        '(event % 2 == 1)'
        '&& Pass_nominal'
        '&& usingBEnriched'
        '&& Jet_bReg[hJetInd1]>25'
        '&& Jet_bReg[hJetInd2]>25'
        '&& (isWenu && (abs(Electron_eta[lepInd1])<1.44 || abs(Electron_eta[lepInd1])>1.56))'
        '&& (controlSample==0 || (controlSample==13 && lepMetDPhi<2 && HVdPhi>2.5))'
        '&& V_pt>150'
        '&& hJets_btagged_0>0.8001'
        '&& hJets_btagged_1>0.1522'
        '&& (H_mass>60 && H_mass<160)'
    )

    print 'Preprocessing file {0}'.format(os.path.basename(path))
    try:
        df = root_pandas.read_root(path, key='Events', columns=['sampleIndex', 'event', 'H_mass'], where=SELECTION)
    except IndexError as e:
        if e.message == 'index 0 is out of bounds for axis 0 with size 0':
            return
        else:
            raise
    # Merge with the friend tree.
    df_friend = (
        root_pandas.read_root(friend, key='Events', columns=['sampleIndex', 'event', 'DNN_nominal', 'weight'])
                   .drop_duplicates(subset=['sampleIndex', 'event'])
    )
    df = (
        df.merge(df_friend, on=['sampleIndex', 'event'], suffixes=('', '_new'))
          .rename(index=str, columns={'DNN_nominal': 'WenMasslessDNN'})
    )
    # Add the is_signal branch.
    df['is_signal'] = df.apply(is_signal, axis=1).astype(numpy.int32)
    # Add the Mjj index branch.
    df['mjj_bin_index'] = df.apply(mjj_bin_index, axis=1).astype(numpy.int32)
    # Save the preprocessed file.
    filename = os.path.splitext(os.path.basename(path))[0]
    with pandas.HDFStore(os.path.join(outdir, filename + '.h5')) as store:
        store['Events'] = df


def remove_duplicates(pattern, outdir):
    # Concatenate together all subdataframes for a sample.
    paths = glob.glob('step1/sum_{0}*.h5'.format(pattern))
    parts = []
    for path in paths:
        parts.append(pandas.read_hdf(path, key='Events'))
    # Search for duplicates in different parts.
    for i, current in enumerate(parts):
        for j in xrange(len(parts)):
            if i == j:
                continue
            else:
                other = parts[j]
                tmp = current.merge(other, on=['sampleIndex', 'event'])
                parts[j] = other[~(other['sampleIndex'].isin(tmp['sampleIndex']) & other['event'].isin(tmp['event']))]
    df = pandas.concat(parts)
    df.to_root(os.path.join(outdir, 'merged_{0}.root'.format(pattern)), key='Events')


if __name__ == '__main__':

    NTUPLES = glob.glob('/eos/uscms/store/group/lpchbb/VHbbAnalysisNtuples/2017V5_June19_unblinding/haddjobs/*.root')
    FRIEND = '/eos/uscms/store/group/lpchbb/VHbbNanoPostProc/2017/V5/DNNFRIENDS/DNN_Mjj_17/Wen_mjj_MC.root'

    # Preprocess the 2017 ntuples in parallel.
    makedirs('step1')
    futures = []
    with concurrent.futures.ProcessPoolExecutor(16) as executor:
        for path in NTUPLES:
            # Don't include data, QCD, or ZJetToNuNu. Also, skip a problematic DY file.
            if 'QCD' in path or 'ZJets' in path or 'ZBJets' in path or 'Run' in path or 'DYToLL_M4to50_HT70' in path:
                continue
            # Skip specific VV samples.
            if 'WW_0.root' in path or 'WW_1.root' in path or 'WW_LNu2Q_nlo' in path or 'WZ.root' in path or 'ZZ.root' in path or 'ZZ_4L' in path:
                continue
            path = path.replace('/eos/uscms', 'root://cmseos.fnal.gov/')
            FRIEND = FRIEND.replace('/eos/uscms', 'root://cmseos.fnal.gov/')
            futures.append(executor.submit(preprocess_for_autocat, path, FRIEND, 'step1'))
        done, not_done = concurrent.futures.wait(futures)

    makedirs('step2')
    patterns = set([os.path.splitext(path)[0].split('sum_')[1].rsplit('_', 1)[0] for path in glob.glob('step1/sum_*.h5')])
    for pattern in patterns:
        print 'Deduplicating sample {0}'.format(pattern)
        remove_duplicates(pattern, 'step2')

    # The parallel preprocessing  produces individual output ROOT files for each ntuple,
    # which are concatenated into a single dataframe for use by the autocategorizer.
    dfs = []
    for path in glob.glob('step2/merged_*.root'):
        dfs.append(uproot.open(path)['Events'].pandas.df())
    df = pandas.concat(dfs)
    df.to_root('autocat_input.root', key='Events')

