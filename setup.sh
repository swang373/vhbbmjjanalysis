#!/usr/bin/env bash
pushd utils/autocategorizer
git clone https://github.com/acarnes/bdt.git
pushd bdt
git checkout binned_categorizer
popd
make
popd
pip install --user wurlitzer
export PYTHONPATH="$PWD/:$PYTHONPATH"
