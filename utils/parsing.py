import glob
import math
import os

import pandas
import wurlitzer

import autocategorizer


def parse_mjj_category_tree(path):
    xml_categorizer = autocategorizer.XMLCategorizer(path)
    with wurlitzer.pipes() as (stdout, stderr):
        xml_categorizer.outputCategories()
    categories = stdout.read().splitlines()[:-2]
    
    significances = []
    splits = []
    for category in categories:
        fields = [field.replace('p', '.').replace('n', '-') for field in category.split('_')]
        significances.append(float(fields[1]))
        for i, field in enumerate(fields[2:]):
            if i % 3 == 2:
                splits.append(float(field))

    categorization_info = {
        'categories': sorted(set(splits)),
        'total_significance': math.sqrt(sum(s**2 for s in significances)),
    }

    return categorization_info


def parse_mjj_category_trees(src, bounds=None):
    columns = {x: [] for x in ['n_categories', 'metric', 'n_minbkg', 'smooth_bkg', 'categories', 'total_significance']}
    for path in glob.glob(os.path.join(src, '*.xml')):
        filename, _ = os.path.splitext(os.path.basename(path))
        # Assume that the filenames follow the convention:
        # mjj_{n_bins}_{metric}_{n_minbkg}_{smooth_bkg}
        fields = filename.split('_')
        columns['n_categories'].append(int(fields[1]))
        columns['metric'].append(fields[2])
        columns['n_minbkg'].append(int(fields[3]))
        columns['smooth_bkg'].append(True if fields[4] == 'True' else False)
        result = parse_mjj_category_tree(path)
        if bounds:
            columns['categories'].append([bounds[0]] + result['categories'] + [bounds[1]])
        else:
            columns['categories'].append(result['categories'])
        columns['total_significance'].append(result['total_significance'])
    df = pandas.DataFrame(columns)
    return df

